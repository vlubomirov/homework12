﻿# Технологично училище "Електронни системи" www.elsys-bg.org 11Б клас
# Василен Асенов Любомиров
# Номер 7
# Задача - Да се създаден калкулатор +/- на числа в полски запис .
class CALC
	def is_a_number?(s)

  	s.to_s.match(/\A[+-]?\d+?(\.\d+)?\Z/) == nil ? false : true

	end	
	def oper(input)
	stack = []
	input=input.split(" ")	
	
		input.each do |d|
			
			if is_a_number?(d)
				stack.push(d.to_i)
		
			else 
			
				if d=="+"
					first=stack.pop()
					second=stack.pop()
					oper=first.to_i + second.to_i
					
					stack.push(oper)
				end
				if d=="-"
					first=stack.pop()
					second=stack.pop()
					oper=first.to_i - second.to_i
					stack.push(oper)
				end
			end


		end
		p stack
		
	end
end

 p = CALC.new
 input=gets
p.oper(input)
